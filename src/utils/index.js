export const getUuid = () => Math.random().toString(16).slice(2)

export const getRandom = (ub) => Math.floor(Math.random() * ub)
