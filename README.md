# SQL-runner

App link: https://sql-runner.vercel.app/

## Overview

SQL-runner is a web-based application capable of running mock SQL queries and displaying the mock results of said query. 

Save feature in app enable users to save queries that can be run at later point of time making data analysts life bit simpler.

Tables are loaded using virtualisation which helps in loading large tables on the go without affecting application performance.


## Features
SQL-runner includes following features:
  - Type your queries in sql editor. Run queries to get result.
  - Save queries with name and description for later use. Can add tags as well.
  - Virtualisation to load large tables without affecting application performance.

  *** 

  
## Major Frameworks, Plugins and packages
  - Vuejs
  - Quasar (Vue.js based framework)
  - CodeMirror (for sql editor)

## Load time
  - Test 1 (env: dev): 
    - Page load time: 22.3 ms
    - Measured using: Vue-dev tools
  - Test 2 (env: prod): 
    - Page load time: 29 ms
    - Measured using: [Page Load Time](https://chrome.google.com/webstore/detail/page-load-time/fploionmjgeclbkemipmkogoaohcdbig?hl=en) (Chrome Extension)
  - Test 3 (env: prod): 
    - Page load time: 52 ms - 186 ms
    - Measured using: [BrowserStack](https://www.browserstack.com/)

    - ![BrowserStack performance report](https://i.ibb.co/dchLhBF/Screenshot-2022-05-28-195306.png)

## Optimisation
  - Virtualisation of query tables using Quasar virtual scroll.

  _* can load large amout of rows without breaking/crashing the browser_